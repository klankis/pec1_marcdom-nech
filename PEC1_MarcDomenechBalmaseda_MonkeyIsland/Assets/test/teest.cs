﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teest : MonoBehaviour
{
    [SerializeField]
    private bool testBool;
    [SerializeField]
    private int testInt;
    [SerializeField]
    private float testFloat;
    [SerializeField]
    private string testString;

    private void Start()
    {
        Debug.Log($"TestBool = {testBool}");
        Debug.Log($"TestInt = {testInt}");
        Debug.Log($"TestFloat = {testFloat}");
        Debug.Log($"TestString = {testString}");

    }
}

