﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StartGame : MonoBehaviour
{
    private int valorInici;
    public Text textinsults;
        
    private string insultia;
    //public GameObject respostajugador1;
    private int respostaia;

    public GameObject[] vidaPJ;
    public GameObject[] vidaIA;

    public string[] Insults;
    public string[] Respostes;

    public GameObject menuP;
    public GameObject menuR;

    private int contadorVidaJugador = 3;
    private int contadorVidaIA = 3;

    
    //Tria a l'inici qui comença a preguntar i va a la funcio Torn
    void Start()
    {
        //Random.Range sempre s'ha d'afegir 1+ perque sigui entre 2 opcions ja que no s'inclou el maxim al range
        valorInici = Random.Range(0, 2);
        Torn();
    }

    //En el moment en que algun comptador de vida arribi a 0 cambiara d'escena
    void Update()
    {
       
        if (contadorVidaIA == 0)
        {
            SceneManager.LoadScene("Win");
        }
        if (contadorVidaJugador == 0)
        {
            SceneManager.LoadScene("Game Over");
        }
    }

    //quan apretem un boto de les respostes quan ens pregunti la IA sabrem si es correcte o no
    public void Resposta(int contestacio)
    {
        if (contestacio == System.Array.IndexOf(Insults,insultia))
        {
            contadorVidaIA--;            
            UpdateVidaIA();
            valorInici = 1;
        }
        else
        {
            contadorVidaJugador--;            
            UpdateVidaJugador();
            valorInici = 0;
        }
        
        menuR.SetActive(false);
        menuP.SetActive(false);
        StartCoroutine(EsperarPerAmagarText());
    }
    //quan apretem un boto de les preguntes la IA respondra amb un 50% de probavilitat la resposta correcta i la mostrara en pantalla
    public void Insult(int pregunta)
    {
        respostaia = Random.Range(0, 1);

        if (respostaia == 1)
        {
            textinsults.text = Respostes[pregunta];
            contadorVidaJugador--;
            valorInici = 0;
            UpdateVidaJugador();
        }
        else
        {
            // mentre no trobi un valor que no sigui igual al de la pregunta buscara un random de la llista
            int preguntaActual = -1;
            do
            {
                preguntaActual = Random.Range(0, Respostes.Length);
            }
            while (preguntaActual == pregunta);

            textinsults.text = Respostes[preguntaActual];
            contadorVidaIA--;
            valorInici = 1;
            UpdateVidaIA();
        }
        menuR.SetActive(false);
        menuP.SetActive(false);
        StartCoroutine(EsperarPerAmagarText());
        
    }

    //desactiva el GameObject de la vida de la IA o jugador
    void UpdateVidaIA()
    {
        vidaIA[contadorVidaIA].SetActive(false);
    }
    void UpdateVidaJugador()
    {
        vidaPJ[contadorVidaJugador].SetActive(false);
    }
    

    
    IEnumerator EsperarPerAmagarText()
    {
        yield return new WaitForSeconds(3);
        textinsults.text = string.Empty;
        Torn();
    }
    

    //Depenent del valor inicial mostra el menu de insults o respostes, en el cas de respostes (0) tria un insult
    void Torn()
    {
        if (valorInici == 0)
        {
            insultia = textinsults.text = Insults[Random.Range(0,Insults.Length)];
            menuR.SetActive(true);
            menuP.SetActive(false);
        }

        if (valorInici == 1)
        {
            menuR.SetActive(false);
            menuP.SetActive(true);
        }
    } 
    
}
