Link video Youtube:

https://youtu.be/0nDKpNMSG9s

**COMO JUGAR:**

Al inicio del juego se decide quien empieza el duelo e insulta al rival.
Si te toca iniciar elige un insulto y la maquina intentara responder, si acierta la respuesta correcta te bajara 1 punto, de los 3 que tienes, y le tocara insultarte, en el casual que falle perderá una vida y volverás a preguntar.
Si le toca insultar a la maquina te mostrara un insulto y tu tienes que elegir una respuesta de las 16 que se te mostraran, si es correcta le bajas 1 punto y te tocara insultar, de lo contrario perderás una vida y volverá a preguntar.
El primero que llegue a 0 vidas pierde.

**ESTRUCTURA DEL JUEGO:**

El Juego consta de 4 escenas:
-Menu
-Juego
-Ganador
-Perdedor

Las escenas de ganador y perdedor son las mismas pero se diferencian del mensaje de YOU WIN / GAME OVER dependiendo de como termine el duelo y con 2 botones; volver al menú y salir del juego.
La escena de Menú cuenta con 2 botones; Jugar y salir del juego.
La escena de Juego es donde se va a desarroyar todo el juego. En el encontraremos 2 menus, uno con todos los botones de los insultos y otro con las respuestas, un texto donde se mostrara los insultos y las respuestas de la maquina y las vidas de cada uno.

Se ha creado un solo script para todo el codigo.

